--a. find all artists that has letter d in its name.

select * from artists where artist_name like "%d%"; 

--b. find all songs that has length of less than 230.

select * from songs where length <=230;

--c. join the 'albums' and songs tables. (Only show the album name, song name and song length)

select * from albums 
	 join songs on albums.id = albums.song_id
	 join song on songs.id = songs.song_name_id
	 join song on songs.id=songs.song_length_id;

--d. join the 'artists' and 'albums' table (Find all albums that has letter a in its name)


select * from artists 
	 join albums on artists.id = albums.artist_id; 

select * from albums where album_title like "%a%"; 


--e. Sort the albums in Z-A order. Show only the first 4 records.


select * from songs order by song_name DESC;

--f. join the 'albums' and 'songs' tables. (Sort albums from Z-A.)

select * from songs 
	 left join albums on songs.id = albums.song_id; 

select * from songs order by song_name DESC;